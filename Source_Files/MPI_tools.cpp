#include <iostream>
#include <vector>
#include <mpi.h>
#include "MPI_tools.hpp"

using std::vector;

void MPI_division
    (
    // ======================================================== //
    int length,
    int StartPoint,
    int &start, 
    int &end, 
    int &count,
    vector<int> &MPIstart,
    vector<int> &MPIend,
    vector<int> &MPIcount,
    int myid,
    int nproc

    // ======================================================== //
    
    )
{
    // ======================================================== //
    int Xdv = length / nproc;				    
    int Xr = length - Xdv * nproc;
    // ======================================================== //

    MPIstart.reserve(nproc);
    MPIend.reserve(nproc);
    MPIcount.reserve(nproc);


    for(size_t i=0; i<nproc; ++i)
    {
        if(i < Xr)
        {
            MPIstart.push_back( i * (Xdv + 1) + StartPoint );			
            MPIend.push_back( MPIstart[i]+ Xdv );
        }
        else
        {
            MPIstart.push_back( i * Xdv + Xr + StartPoint );		

            MPIend.push_back( MPIstart[i] + Xdv - 1 );		

        }

        MPIcount.push_back( MPIend[i] - MPIstart[i] + 1 );	

    }


    start = MPIstart[myid];
    end = MPIend[myid];
    count = MPIcount[myid];

   

}


vector<double> MPI_Collect
    (
    // ======================================================== //
    vector<double> x,
    MPI_tools MPI,
    size_t total_length,
    size_t itag,
    int myid,
    int nproc

    // ======================================================== //
    )
{
    MPI_Status istat[8];
    size_t master{0};
    size_t istart{0};
    size_t icount{0};  



    // data collect among nodes 
    // ======================================================== //
    icount = MPI.count;
    istart = MPI.start;

    
    if( myid > master )
    {
        
        MPI_Send(   (void *)&x[istart], icount, MPI_DOUBLE, master, itag, MPI_COMM_WORLD  );
       
    }
    else if(myid == master)
    {
        for(size_t i = 1 ; i < nproc ; ++i)
        {
            
            MPI_Recv(   (void *)&x[ MPI.start_list[i] ], icount, MPI_DOUBLE, i, itag, MPI_COMM_WORLD, istat  );
           
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    icount = total_length;
    MPI_Bcast(  (void *)&x[0], icount, MPI_DOUBLE, master, MPI_COMM_WORLD   );
    // ======================================================== //

    return x;
}





