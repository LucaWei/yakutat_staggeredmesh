
#ifndef _ARRAY_
#define _ARRAY_

#include <vector>

// #include "csr_matrix.hpp"
#include "Resolution.h"
#include "Variables.h"
#include "hyb2_matrix.hpp"

    
using std::vector;

    
    //Physical matrix
    // ======================================================== //

    vector<double> P               (iceltot);
    
    vector<vector<double> > U      (3,vector<double>(iceltot) );

    vector<vector<double> > U2     (3,vector<double>(iceltot) );

    vector<vector<double> > U_star (3,vector<double>(iceltot) );
    
    vector<double> ETA             (iceltot);

    vector<vector<double> > FX     (3,vector<double>(iceltot) );


    
   
    // ======================================================== //

    
    //Grid matrix
    // ======================================================== //

    
    vector<double> iDx            (nx);
    vector<double> Dxs            (nx);
    vector<double> iDy            (ny);
    vector<double> Dys            (ny);
    vector<double> iDz            (nz);
    vector<double> Dzs            (nz);



    // ======================================================== //

    // Define neighbor of U P array
    // ======================================================== //

    vector<vector<double> > NEIBcell (12,vector<double>(iceltot) );
    
    // ======================================================== //


    // BiCGSTAB
    // ======================================================== //

    MatOps::SparseMatrixHYB<double> matA((nx-2*gCells)* (ny-2*gCells) *(nz-2*gCells), 100, myid, nproc);
    vector<double>          B_coo_val;
    vector<double>          x((nx-2*gCells)* (ny-2*gCells) *(nz-2*gCells));

    // ======================================================== //
 


#endif