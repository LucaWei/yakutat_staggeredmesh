#ifndef _CONVECTIONSCHEME_INCLUDE_
#define _CONVECTIONSCHEME_INCLUDE_

#include <vector>

using std::vector;

void DiscretisationQUICK
    (
        MPI_tools &U_MPI,
        size_t num_threads,
        double dt, double nu,

        vector<vector<double> > &U,
        vector<vector<double> > &U_star,
        vector<vector<double> > &NEIBcell,

        vector<double> &iDx,
        vector<double> &Dxs,
        vector<double> &iDy,
        vector<double> &Dys,
        vector<double> &iDz,
        vector<double> &Dzs

    
    );






#endif