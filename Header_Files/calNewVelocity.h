#ifndef _CALNEWVELOCITY_INCLUDE_
#define _CALNEWVELOCITY_INCLUDE_

#include <vector>

using std::vector;

void calNewVelocity
    (
        double dt, 
        MPI_tools U_MPI,

        vector<double> &P,
        vector<vector<double> > &U_star,
        vector<vector<double> > &U2,
        vector<vector<double> > &FX,
        vector<double> &ETA,


        vector<double> &iDx,
        vector<double> &Dxs,
        vector<double> &iDy,
        vector<double> &Dys,
        vector<double> &iDz,
        vector<double> &Dzs

    
    );

void updating
    (

        vector<vector<double> > &U,
        vector<vector<double> > &U2,
        MPI_tools U_MPI

    );




#endif
